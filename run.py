from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
import base64
import time
from PIL import Image
import json
import random

def set_viewport_size(driver, width, height):
    window_size = driver.execute_script("""
        return [window.outerWidth - window.innerWidth + arguments[0],
          window.outerHeight - window.innerHeight + arguments[1]];
        """, width, height)
    driver.set_window_size(*window_size)




window_width = 1200
window_height = 800

frames = []

DRIVER = '/Users/alexroidl/projects/xd/chromedriver'
driver = webdriver.Chrome(DRIVER)
#driver.set_window_size(window_width, window_height)
set_viewport_size(driver, window_width, window_height)



with open("sketches") as f:
    content = f.readlines()
# you may also want to remove whitespace characters like `\n` at the end of each line
sketches = [x.strip() for x in content] 

for url in sketches:
	sketchid = url.rsplit('/', 2)[-2]
	driver.get(url)
	time.sleep(5)
	try:
		driver.find_element_by_css_selector("#onetrust-accept-btn-handler").click();
	except:
		print("notfound")
	time.sleep(2)
	try:
		driver.find_element_by_css_selector(".cs-close").click();
	except:
		print("notfound")

	list = driver.execute_script("return window.prototypeData;")
	all_artboards = list["manifest"]["artboards"]

	selected_artboards = []
	selected_artboards.append(all_artboards[0])
	selected_artboards.extend(random.sample(all_artboards,4))

	for board in selected_artboards:
		id_screen = board["id"]

		driver.get(url + '/screen/'+id_screen)

		time.sleep(3)

		screenshot = driver.save_screenshot('screenshot.png')


		app_width = list["manifest"]["artboards"][0]["bounds"]["width"]
		app_height = list["manifest"]["artboards"][0]["viewport"]["height"]

		ratio = window_height / app_height
		actual_width = app_width * ratio 
		print(actual_width)

		x = window_width/2-actual_width/2
		y = 0
		width = window_width/2-actual_width/2 + actual_width
		height = window_height

		im = Image.open('screenshot.png')
		im = im.resize((window_width, window_height), Image.ANTIALIAS)
		im = im.crop((int(x), int(y), int(width), int(height)))
		frames.append(im)

	# #im.save('image.png')

	frames[0].save(sketchid+'.gif', format='GIF', append_images=frames[1:], save_all=True, duration=1000, loop=0)
	frames = []

driver.quit()


